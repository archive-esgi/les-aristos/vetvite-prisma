const { forwardTo } = require('prisma-binding');

async function createAvailability(parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateAvailability(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function deleteAvailability(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createAvailability,
    updateAvailability,
    deleteAvailability
}