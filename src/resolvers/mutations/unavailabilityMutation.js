const { forwardTo } = require('prisma-binding');

async function createUnavailability(parent, args, ctx, info) {
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateUnavailability(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function deleteUnavailability(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    createUnavailability,
    updateUnavailability,
    deleteUnavailability
}