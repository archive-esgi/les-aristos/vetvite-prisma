const {forwardTo} = require('prisma-binding');

async function unavailability(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info);
}

async function unavailabilities(parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info);
}   

module.exports = {
    unavailability,
    unavailabilities
}