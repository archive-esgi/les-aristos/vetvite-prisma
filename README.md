<div align="center">
    <img src="logo.png" width="170"/>
</div>

# Vetvite Prisma

> Vetvite Prisma is the GraphQL API used to manage professionals availabilities and care types.

## Requirements
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/) (19 <=)
- [Make](https://fr.wikipedia.org/wiki/Make)

## How to setup on local environment ?

```sh
make run-local
```

GraphQL Playground can be found at http://localhost:4466.

## How to deploy Prisma configuration ?
Prisma configuration is deploy on containers launch but if you want deploy while there are already up you can use this command :
```sh
make prisma-deploy
```

## How to setup on development environment ?
[Traefik](https://gitlab.com/les-aristos/vetvite-traefik) container must be installed and started.

Remember to change the domain in the Traefik label in **docker-compose.dev.yml**.

```sh
make run-dev
```

## How to setup on production environment ?
[Traefik](https://gitlab.com/les-aristos/vetvite-traefik) container must be installed and started.

Remember to change the domain in the Traefik label in **docker-compose.prod.yml**.
```sh
make run-prod
```

## Authors

| <a href="https://gitlab.com/christophele" target="_blank">**Christophe LE**</a> | <a href="https://gitlab.com/kiliandiogo" target="_blank">**Kilian DIOGO**</a> | <a href="https://gitlab.com/nolway" target="_blank">**Alexis FAIZEAU**</a> | <a href="https://gitlab.com/kamISKRANE" target="_blank">**Kamel ISKRANE**</a> | <a href="https://gitlab.com/BaptisteVasseur" target="_blank">**Baptiste Vasseur**</a>
| :---: |:---:|:---:| :---:| :---:|
| <a href="https://gitlab.com/christophele"><img src="https://gitlab.com/uploads/-/system/user/avatar/1408322/avatar.png?width=200" width="200"/></a>  | <a href="https://gitlab.com/kiliandiogo"><img src="https://secure.gravatar.com/avatar/6e610014305ee861da9cb4bd84a6dbba?s=800&d=identicon" width="200"/></a> | <a href="https://gitlab.com/nolway"><img src="https://gitlab.com/uploads/-/system/user/avatar/1241492/avatar.png?width=400" width="200"/></a>  | <a href="https://gitlab.com/kamISKRANE"><img src="https://gitlab.com/uploads/-/system/user/avatar/3603263/avatar.png?width=400" width="200"/></a> | <a href="https://gitlab.com/BaptisteVasseur"><img src="https://gitlab.com/uploads/-/system/user/avatar/3786758/avatar.png?width=400" width="200"/></a>
