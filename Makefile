run-local:
	docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d

run-dev:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d

run-prod:
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

stop:
	docker-compose down

deploy-prisma:
	docker-compose run node npx prisma1 deploy